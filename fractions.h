/*
===========================================================================
Copyright (C) 2012-2013 Abdessamii Raqi

This file is part of mini linear systems resolver (MLSR) source code.

MLSR source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

MLSR source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
 */

/*****************************************************************************
 * name:		fractions.h
 *
 * desc:		headers pour routines de traitement de nombres fractionnels
 *
 * author:      Abdessamii Raqi
 *
 * e-mail:      abdessamii.raqi@gmail.com
 *****************************************************************************/

#ifndef FRACTIONS_H_INCLUDED
#define FRACTIONS_H_INCLUDED

typedef
struct st_fraction {
    long hi;
    long down;
} Fraction;

double f_toDouble(const Fraction* fr1);
float f_toFloat(const Fraction* fr1);
Fraction f_add(const Fraction* fr1, const Fraction* fr2);
Fraction f_sub(const Fraction* fr1, const Fraction* fr2);
Fraction f_multi(const Fraction* fr1, const Fraction* fr2);
Fraction f_divi(const Fraction* fr1, const Fraction* fr2);
void f_rev(Fraction* fr1);
void f_simplify(Fraction* fr1);
long pgdc(const long* nb1, const long* nb2);
#endif // FRACTIONS_H_INCLUDED
