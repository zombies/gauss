/*
===========================================================================
Copyright (C) 2012-2013 Abdessamii Raqi

This file is part of mini linear systems resolver (MLSR) source code.

MLSR source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

MLSR source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
 */

/*****************************************************************************
 * name:		fractions.c
 *
 * desc:		routines de traitement de nombres fractionnels
 *
 * author:      Abdessamii Raqi
 *
 * e-mail:      abdessamii.raqi@gmail.com
 *****************************************************************************/

#include "fractions.h"

double f_toDouble(const Fraction* fr1) {
    return (double) fr1->hi / fr1->down;
}

float f_toFloat(const Fraction* fr1) {
    return (float) fr1->hi / fr1->down;
}

Fraction f_add(const Fraction* fr1, const Fraction* fr2) {
    Fraction result;
    result.down = fr1->down * fr2->down;
    result.hi = (fr1->hi * fr2->down)+(fr2->hi * fr1->down);
    return result;
}

Fraction f_sub(const Fraction* fr1, const Fraction* fr2) {
    Fraction result;
    result.down = fr1->down * fr2->down;
    result.hi = (fr1->hi * fr2->down)-(fr2->hi * fr1->down);
    return result;
}

Fraction f_multi(const Fraction* fr1, const Fraction* fr2) {
    Fraction result;
    result.down = fr1->down * fr2->down;
    result.hi = fr1->hi * fr2->hi;
    return result;
}

Fraction f_divi(const Fraction* fr1, const Fraction* fr2) {
    Fraction fr2_rev;
    fr2_rev.down = fr2->hi;
    fr2_rev.hi = fr2->down;
    return f_multi(fr1, &fr2_rev);
}

void f_rev(Fraction* fr1) {
    long x = fr1->hi;
    fr1->hi = fr1->down;
    fr1->down = x;
}

void f_simplify(Fraction* fr1) {
    long x = pgdc(&fr1->hi, &fr1->down);
    while (x > 1) {
        fr1->down /= x;
        fr1->hi /= x;
        x = pgdc(&fr1->hi, &fr1->down);
    }
}

long pgdc(const long* nb1, const long* nb2) {
    long sml = (*nb1 <= *nb2) ? *nb1 : *nb2;
    long big = (*nb1 <= *nb2) ? *nb2 : *nb1;

    if (big % sml == 0) {
        return sml;
    } else {
        long i = 0;
        for (i = sml; i > 1; i--) {
            if (sml % i == 0 && big % i == 0) {
                return i;
            }
        }
    }
    return 1L;
}
