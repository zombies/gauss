/*
===========================================================================
Copyright (C) 2012-2013 Abdessamii Raqi

This file is part of mini linear systems resolver (MLSR) source code.

MLSR source code is free software; you can redistribute it
and/or modify it under the terms of the GNU General Public License as
published by the Free Software Foundation; either version 2 of the License,
or (at your option) any later version.

MLSR source code is distributed in the hope that it will be
useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Foobar; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
===========================================================================
*/

/*****************************************************************************
 * name:		main.c
 *
 * desc:		main
 *
 * author:      Abdessamii Raqi
 *
 * e-mail:      abdessamii.raqi@gmail.com
 *****************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include "fractions.h"
#include "matrix.h"
#include "lsystems.h"

int main()
{
    Fraction x,y,z;
    x.hi=14;
    x.down=5;
    y.hi=7;
    y.down=15;
    z=f_divi(&x,&y);
    printf("%d/%d\n",(int)z.hi,(int)z.down);
    f_simplify(&z);
    printf("%d/%d --(%d)--> %f\n",(int)z.hi,(int)z.down,(int)pgdc(&z.down,&z.hi),f_toFloat(&z));
    return 0;
}
