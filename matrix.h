#ifndef MATRIX_H_INCLUDED
#define MATRIX_H_INCLUDED

typedef long** matrix2d;
typedef long*** matrix3d;
matrix2d m_add(matrix2d* m2d1, matrix2d* m2d2);
matrix2d m_scalmulti(long x, matrix2d* m2d1);
matrix2d m_multi(matrix2d* m2d1, matrix2d* m2d2);
#endif // MATRIX_H_INCLUDED
